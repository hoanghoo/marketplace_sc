const fs = require("fs");
const readline = require("readline");
const os = require("os");
const { exec } = require("child_process");

const second = (n) => {
  return n;
};
const minute = (n) => {
  return n * second(60);
};
const hour = (n) => {
  return n * minute(60);
};

const log = (...args) => {
  console.log(`${new Date().toLocaleTimeString()}\t ${args}`);
};
var recipient = [];
var mnemonic = [];
let priv = [];

// read mother wallet's information
const file = "wallets.txt";

const offerCommand = "npx hardhat run ./scripts/createOffer.ts "; // for testing, add --network auroraMainnet for mainnet

const withdrawCommand =
  "npx hardhat run ./scripts/withdraw.ts --network auroraMainnet";

const buyOfferCommand =
  "npx hardhat run ./scripts/buyOffer.ts --network auroraMainnet";

const sellersFileName = "sellers.txt";

const maxIndex = 25;

const minOfferPrice = 249;
const maxOfferPrice = 500;

const minSleep = minute(2);
const maxSleep = minute(3);

function randomNum(min, max) {
  return Math.floor(Math.random() * (max - min)) + min; // You can remove the Math.floor if you don't want it to be an integer
}

function writeEnv(
  mnemonic,
  mnemonicIndex,
  offerPrice,
  recipient,
  sellersFileName
) {
  const strEnv = `MNEMONIC=${mnemonic}${
    os.EOL
  }OFFER_PRICE=${offerPrice.toString()}${
    os.EOL
  }MNEMONIC_INDEX=${mnemonicIndex.toString()}${
    os.EOL
  }RECIPIENT=${recipient.toString()}${
    os.EOL
  }SELLERS_FILE_NAME=${sellersFileName}`;

  fs.writeFileSync(".env", strEnv);
}

async function makeOffer() {
  const numOfWallets = mnemonic.length;

  for (let i = 0; i < numOfWallets; ++i) {
    for (let j = 0; j < maxIndex; ++j) {
      const randomPrice = randomNum(minOfferPrice, maxOfferPrice);
      log(
        `make offer with ${mnemonic[i]} index ${j} with price ${randomPrice}`
      );

      writeEnv(mnemonic[i], j, randomPrice, recipient[i], sellersFileName);

      exec(
        offerCommand + ` >> logs/offers-${i}-${j}.log`,
        (err, stdout, stderr) => {
          if (err) {
            // node couldn't execute the command
            log(err.message);
            return;
          }
          // the *entire* stdout and stderr (buffered)
          console.log(`stdout: ${stdout}`);
          console.log(`stderr: ${stderr}`);
        }
      );
      const timeToSleep = randomNum(minSleep, maxSleep) * 1000;
      console.log(`sleep for ${timeToSleep / 1000} seconds`);
      await sleep(timeToSleep); // sleep
    }
  }
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function readWallets() {
  const fileStream = fs.createReadStream(file);

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.

  for await (const line of rl) {
    // Each line in mother_wallets.txt will be successively available here as `line`.
    const result = line.split(":");
    priv.push(result[0]);
    mnemonic.push(result[1]);
    recipient.push(result[2]);
  }
  makeOffer();
}

log("MAKE OFFERS STARTS!!");
readWallets();
