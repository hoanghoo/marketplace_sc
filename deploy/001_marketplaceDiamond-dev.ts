import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;

  const { diamond } = deployments;
  const { deployerTest, feeCollectorTest } = await getNamedAccounts();

  await diamond.deploy("Marketplace-Dev", {
    from: deployerTest,
    owner: deployerTest,
    facets: [
      "DirectSaleFacet",
      "ERC721ReceiverFacet",
      "CurrencyFacet",
      "WithdrawalFacet",
    ],
    execute: {
      methodName: "init",
      contract: "MarketplaceInit",
      args: [
        {
          feeCollector: feeCollectorTest,
          listingFee: 300,
          tokens: ["0x0c984Ab77f34Fd4678095A3021c0E4959965af44"],
        },
      ],
    },
    autoMine: true,
    log: true,
  });
};
export default func;
func.tags = ["Marketplace-Dev"];
