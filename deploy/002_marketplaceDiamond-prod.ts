import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;

  const { diamond } = deployments;
  const { deployerBeta, feeCollectorBeta } = await getNamedAccounts();

  await diamond.deploy("MarketplaceDiamond-Staging", {
    from: deployerBeta,
    owner: deployerBeta,
    facets: [
      "DirectSaleFacet",
      "ERC721ReceiverFacet",
      "CurrencyFacet",
      "WithdrawalFacet",
    ],
    // execute: {
    //   methodName: "init",
    //   contract: "MarketplaceInit",
    //   args: [
    //     {
    //       feeCollector: feeCollectorBeta,
    //       listingFee: 300,
    //       tokens: ["0x55d398326f99059ff775485246999027b3197955"],
    //     },
    //   ],
    // },
    autoMine: true,
    log: true,
  });
};
export default func;
func.tags = ["MarketplaceDiamond-Staging"];
