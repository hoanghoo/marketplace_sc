//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;

import "hardhat-deploy/solc_0.8/diamond/UsingDiamondOwner.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";
bytes32 constant DIAMOND_STORAGE_POSITION = keccak256(
    "diamond.standard.diamond.storage"
);

uint256 constant ENTERED = 2;
uint256 constant NOT_ENTERED = 1;
struct FacetAddressAndPosition {
    address facetAddress;
    uint96 functionSelectorPosition;
}
struct FacetFunctionSelectors {
    bytes4[] functionSelectors;
    uint256 facetAddressPosition;
}
struct DiamondStorage {
    mapping(bytes4 => FacetAddressAndPosition) selectorToFacetAndPosition;
    mapping(address => FacetFunctionSelectors) facetFunctionSelectors;
    address[] facetAddresses;
    mapping(bytes4 => bool) supportedInterfaces;
    address contractOwner;
}

struct SaleOffer {
    address hostContract;
    uint256 tokenId;
    address seller;
    uint256 price;
    address currency;
    uint16 feePercent;
    bool closed;
}

library LibDiamondStorage {
    function diamondStorage() internal pure returns (DiamondStorage storage s) {
        bytes32 position = DIAMOND_STORAGE_POSITION;
        assembly {
            s.slot := position
        }
    }
}

struct AppStorage {
    uint256 nonce;
    uint16 listingFee;
    mapping(uint256 => SaleOffer) offers;
    EnumerableSet.UintSet openningIds;
    uint256 entrant;
    bool supportsEther;
    EnumerableSet.AddressSet currencySet;
    mapping(address => uint256) ethBalances;
    mapping(address => mapping(address => uint256)) erc20Balances;
    address feeCollector;
}

contract UsingAppStorage is UsingDiamondOwner {
    AppStorage internal s;

    function contractOwner() internal view returns (address) {
        return LibDiamondStorage.diamondStorage().contractOwner;
    }
}
