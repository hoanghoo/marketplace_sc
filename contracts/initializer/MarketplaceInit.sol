//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "../libraries/LibAppStorage.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

contract MarketplaceInit is UsingAppStorage {
    using EnumerableSet for EnumerableSet.AddressSet;
    struct InitData {
        address feeCollector;
        uint16 listingFee;
        address[] tokens;
    }

    function init(InitData calldata _input) external {
        s.feeCollector = _input.feeCollector;
        s.listingFee = uint16(_input.listingFee);
        for (uint256 i = 0; i < _input.tokens.length; i++) {
            s.currencySet.add(_input.tokens[i]);
        }
    }
}
