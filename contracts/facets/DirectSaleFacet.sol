//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "../libraries/LibAppStorage.sol";

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/interfaces/IERC721.sol";
import "@openzeppelin/contracts/interfaces/IERC721Receiver.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

/// @title Direct Sale Facet
/// @author 0xc0ffee
/// @notice A simple application for user to order/buy an ERC721 token
contract DirectSaleFacet is UsingAppStorage {
    using SafeERC20 for IERC20;
    using SafeMath for uint256;
    using EnumerableSet for EnumerableSet.UintSet;
    using EnumerableSet for EnumerableSet.AddressSet;

    /// @notice Sell offer created, if currency is address(0), then it must be bought by ether
    /// @dev Details of the offer can be fetched with offerId
    /// @param offerId id of the offer
    /// @param hostContract contract address of ERC721 token
    /// @param tokenId tokenId
    /// @param seller who created the offer
    /// @param currency ERC20 used as currency
    /// @param price in unit of ERC20 above
    event OfferCreated(
        uint256 offerId,
        address indexed hostContract,
        uint256 tokenId,
        address indexed seller,
        address indexed currency,
        uint256 price
    );

    /// @notice Offer closed by a buyer
    /// @dev Details of the offer can be fetched with offerId
    /// @param offerId id of the offer
    /// @param seller who created the offer
    /// @param currency ERC20 used as currency
    /// @param buyer who bought the offer
    /// @param price in unit of currency
    event OfferClosed(
        uint256 offerId,
        address indexed seller,
        address indexed buyer,
        address indexed currency,
        uint256 price
    );

    /// @notice Offer closed by the seller
    /// @dev Details of the offer can be fetched with offerId
    /// @param offerId id of the offer
    event OfferCancelled(uint256 offerId);

    /// @notice Offer price was replaced
    /// @dev The offer with old price will be replaced by a new offer with new price
    /// @param offerId the previous id of offer
    /// @param newOfferId the new id of offer whose price replaced
    event OfferReplaced(uint256 offerId, uint256 newOfferId);

    /// @notice Listing fee changed by owner, fee is in percent, i.e 20% => 2000
    /// @dev All offers that created before this event stay unchanged
    /// @param fee new listing fee, in percent
    event ChangeListingFee(uint16 fee);

    modifier nonReentrant() {
        require(s.entrant != ENTERED, "Reentrancy guard");
        s.entrant = ENTERED;
        _;
        s.entrant = NOT_ENTERED;
    }

    /// @notice Create an offer to sell a ERC721 token
    /// @dev Explain to a developer any extra details
    /// @param hostContract address of ERC721 contract
    /// @param tokenId to sell
    /// @param price in unit of currencyToken
    /// @param currencyToken ERC20 token used as currency for the order
    function createOffer(
        address hostContract,
        uint256 tokenId,
        uint256 price,
        address currencyToken
    ) external nonReentrant {
        // CHECKS
        require(_supportsCurrency(currencyToken), "Currency not supported");
        require(price > 0, "Price must be positive");
        require(
            IERC721(hostContract).supportsInterface(type(IERC721).interfaceId),
            "Host contract is not supported"
        );

        require(
            IERC721(hostContract).getApproved(tokenId) == address(this) ||
                IERC721(hostContract).isApprovedForAll(
                    msg.sender,
                    address(this)
                ),
            "Token has not been approved"
        );

        // EFFECTS

        // increase nonce
        s.nonce = s.nonce.add(1);
        // create offer
        s.offers[s.nonce] = SaleOffer({
            hostContract: hostContract,
            tokenId: tokenId,
            seller: msg.sender,
            price: price,
            currency: currencyToken,
            feePercent: listingFee(),
            closed: false
        });
        // mark offerId as openning
        s.openningIds.add(s.nonce);

        // INTERACTIONS
        IERC721(hostContract).transferFrom(msg.sender, address(this), tokenId);

        emit OfferCreated(
            s.nonce,
            hostContract,
            tokenId,
            msg.sender,
            currencyToken,
            price
        );
    }

    /// @notice Cancel an openning offer
    /// @dev This mark the offer closed and transfer back to the seller
    /// @param offerId Id of the offer
    function cancelOffer(uint256 offerId) external nonReentrant {
        // CHECKS
        require(_openning(offerId), "Offer invalid");

        require(s.offers[offerId].seller == msg.sender, "Only seller allowed");

        // EFFECTS
        s.offers[offerId].closed = true;
        s.openningIds.remove(offerId);

        // INTERACTIONS
        IERC721(s.offers[offerId].hostContract).safeTransferFrom(
            address(this),
            s.offers[offerId].seller,
            s.offers[offerId].tokenId,
            ""
        );
        emit OfferCancelled(offerId);
    }

    /// @notice Change price of an offer
    /// @dev This function creates new offer with new nonce to prevent front-running
    /// @param offerId Id of the offer
    /// @param newPrice new price of the offer, must be greater than 0 and different from the previous price
    function changeOfferPrice(uint256 offerId, uint256 newPrice) external {
        // CHECKS
        require(_openning(offerId), "Offer invalid");

        require(s.offers[offerId].seller == msg.sender, "Only seller allowed");
        require(
            newPrice > 0 && newPrice != s.offers[offerId].price,
            "New price invalid"
        );

        // EFFECTS
        // create new offer
        s.nonce = s.nonce.add(1);
        s.offers[s.nonce] = s.offers[offerId];
        s.offers[s.nonce].price = newPrice;
        // remove old
        s.offers[offerId].closed = true;
        s.openningIds.remove(offerId);
        s.openningIds.add(s.nonce);

        emit OfferReplaced(offerId, s.nonce);
        emit OfferCreated(
            s.nonce,
            s.offers[s.nonce].hostContract,
            s.offers[s.nonce].tokenId,
            s.offers[s.nonce].seller,
            s.offers[s.nonce].currency,
            s.offers[s.nonce].price
        );
    }

    /// @notice Buy an offer, the seller pay the listing fee
    /// @dev The token wrapped in the offer is transfered to msg.sender and the price of offer is transfered from msg.sender to this contract , also enable reentrant guard
    /// @param offerId id of the offer
    function buyOffer(uint256 offerId) external nonReentrant {
        // CHECKS
        require(_openning(offerId), "Offer invalid");

        require(
            s.offers[offerId].currency != address(0),
            "Offer could not be paid by ether"
        );
        require(
            msg.sender != s.offers[offerId].seller,
            "Seller is not allowed"
        );
        require(
            IERC20(s.offers[offerId].currency).allowance(
                msg.sender,
                address(this)
            ) >= s.offers[offerId].price,
            "Allowance not enough"
        );
        // EFFECTS

        uint256 fee = calculateOfferFee(
            s.offers[offerId].price,
            s.offers[offerId].feePercent
        ); // fee cut

        s.offers[offerId].closed = true;
        s.openningIds.remove(offerId);

        s.erc20Balances[s.offers[offerId].currency][
            s.offers[offerId].seller
        ] = s
        .erc20Balances[s.offers[offerId].currency][s.offers[offerId].seller]
            .add(s.offers[offerId].price.sub(fee)); // seller gets (price - fee)
        s.erc20Balances[s.offers[offerId].currency][s.feeCollector] = s
        .erc20Balances[s.offers[offerId].currency][s.feeCollector].add(fee); // owner gets fee

        // INTERACTIONS
        IERC721(s.offers[offerId].hostContract).safeTransferFrom(
            address(this),
            msg.sender,
            s.offers[offerId].tokenId,
            ""
        );

        IERC20(s.offers[offerId].currency).safeTransferFrom(
            msg.sender,
            address(this),
            s.offers[offerId].price
        );

        emit OfferClosed(
            offerId,
            s.offers[offerId].seller,
            msg.sender,
            s.offers[offerId].currency,
            s.offers[offerId].price
        );
    }

    /// @notice Buy offer using ether as currency
    /// @dev if msg.value is greater than the offer price, then buyer can withdraw that amount later
    /// @param offerId id of the offer
    function buyOfferWithETH(uint256 offerId) external payable nonReentrant {
        // CHECKS
        require(_openning(offerId), "Offer invalid");

        require(
            s.offers[offerId].currency == address(0),
            "Offer must be paid by ether"
        );
        require(
            msg.sender != s.offers[offerId].seller,
            "Seller is not allowed"
        );
        require(msg.value >= s.offers[offerId].price, "Ether value not enough");

        // EFFECTS
        uint256 fee = calculateOfferFee(
            s.offers[offerId].price,
            s.offers[offerId].feePercent
        ); // fee cut to owner

        s.offers[offerId].closed = true;
        s.openningIds.remove(offerId);

        s.ethBalances[s.offers[offerId].seller] = s
            .ethBalances[s.offers[offerId].seller]
            .add(s.offers[offerId].price.sub(fee)); // seller's pending balance increases by(price - fee)
        s.ethBalances[s.feeCollector] = s.ethBalances[s.feeCollector].add(fee); // owner's pending balance increases by (fee)
        s.ethBalances[msg.sender] = s.ethBalances[msg.sender].add(
            msg.value.sub(s.offers[offerId].price)
        ); // buyer's pending balance incrrease by (msg.value - price)
        // INTERACTIONS
        IERC721(s.offers[offerId].hostContract).safeTransferFrom(
            address(this),
            msg.sender,
            s.offers[offerId].tokenId,
            ""
        );
        emit OfferClosed(
            offerId,
            s.offers[offerId].seller,
            msg.sender,
            s.offers[offerId].currency,
            s.offers[offerId].price
        );
    }

    /// @notice Owner change listing fee, this new fee will not affect all offers that created before this change
    /// @dev fee is in format that, i.e 5% => 500
    /// @param fee new fee
    function changeListingFee(uint16 fee) external onlyOwner {
        s.listingFee = fee;
        emit ChangeListingFee(fee);
    }

    /// @notice Fee collector of the marketplace
    /// @dev This address receive all the fee in the marketplace
    /// @return address of fee collector
    function feeCollector() external view returns (address) {
        return s.feeCollector;
    }

    /// @notice All offer ids of the openning offers
    /// @dev The ids can be used to retrieve offer details
    /// @return returns array of openning offer ids
    function openningIds() external view returns (uint256[] memory) {
        return s.openningIds.values();
    }

    /// @notice Total offer in marketplace, included offer was replaced
    function totalSales() external view returns (uint256) {
        return s.nonce;
    }

    /// @notice Current listing fee that the new sell offer has to accept
    function listingFee() public view returns (uint16) {
        return s.listingFee;
    }

    /// @notice Calculate fee the seller has to pay
    /// @dev Can be used before use createOffer
    /// @param amount the price of the offer that will be listed
    /// @param feePercent feePercent input, should be fetch from listingFee()
    /// @return return fee that seller has to pay
    function calculateOfferFee(uint256 amount, uint16 feePercent)
        public
        pure
        returns (uint256)
    {
        return amount.mul(feePercent).div(10000);
    }

    /// @notice Get offer details
    /// @dev Get offer details
    /// @param offerId id of the offer
    /// @return offer that has id `offerId`
    function getOffer(uint256 offerId)
        external
        view
        returns (SaleOffer memory)
    {
        return s.offers[offerId];
    }

    /// @dev Check whether currency is enabled for listing
    /// @param currency address of ERC20, or ether if currency = address(0)
    /// @return true of false
    function _supportsCurrency(address currency) private view returns (bool) {
        return s.currencySet.contains(currency);
    }

    /// @dev check whether offer is openning for sale
    /// @param offerId id of the offer
    /// @return true or false
    function _openning(uint256 offerId) private view returns (bool) {
        return s.openningIds.contains(offerId);
    }
}
