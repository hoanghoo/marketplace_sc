//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "../libraries/LibAppStorage.sol";
import "@openzeppelin/contracts/interfaces/IERC721Receiver.sol";

/// @title ERC721 Receiver Facet
/// @author 0xc0ffee
/// @notice ERC721Receiver interface
/// @dev Can be used with IERC721.safeTransferFrom
contract ERC721ReceiverFacet {
    /// @notice for IERC721Receiver interface
    /// @dev Used by safeTransferFrom
    function onERC721Received(
        address,
        address,
        uint256,
        bytes memory
    ) external pure returns (bytes4) {
        return IERC721Receiver.onERC721Received.selector;
    }
}
