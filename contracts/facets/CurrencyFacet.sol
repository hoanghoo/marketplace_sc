//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "../libraries/LibAppStorage.sol";
import "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

/// @title Underlying currency facet
/// @author 0xc0ffee
/// @notice Facet use for tracking underlying currency of the marketplace
contract CurrencyFacet is UsingAppStorage {
    using EnumerableSet for EnumerableSet.AddressSet;
    event CurrencySet(address indexed tokenAddress, bool flag);

    /// @notice Add currency to be used in marketplace
    /// @dev tokenAddress equals address(0) means Ether
    /// @param tokenAddress address of ERC20 token, or ether if tokenAddress=address(0)
    function addCurrency(address tokenAddress) external onlyOwner {
        s.currencySet.add(tokenAddress);
        emit CurrencySet(tokenAddress, true);
    }

    function removeCurrency(address tokenAddress) external onlyOwner {
        s.currencySet.remove(tokenAddress);
        emit CurrencySet(tokenAddress, false);
    }

    /// @notice Check whether currency is enabled for usage
    /// @dev tokenAddress equals address(0) means Ether
    /// @param tokenAddress address of ERC20 token, or ether if tokenAddress=address(0)
    /// @return true or false
    function supportsCurrency(address tokenAddress)
        external
        view
        returns (bool)
    {
        return s.currencySet.contains(tokenAddress);
    }
}
