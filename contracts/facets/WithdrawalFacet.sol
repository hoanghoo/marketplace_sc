//SPDX-License-Identifier: Unlicense
pragma solidity 0.8.10;
import "../libraries/LibAppStorage.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

/// @title Withdrawal Facet
/// @author 0xc0ffee
/// @notice Users' fund should be withdrawn from this facet
/// @dev Pull over push pattern
contract WithdrawalFacet is UsingAppStorage {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    event WithdrawETH(address indexed account, uint256 amount);
    event WithdrawERC20(
        address indexed account,
        address indexed token,
        uint256 amount
    );

    /// @notice withdraw all pending Ether
    /// @dev msg.sender is the beneficiary
    function withdrawETH() external {
        uint256 amount = s.ethBalances[msg.sender];
        s.ethBalances[msg.sender] = 0;
        payable(msg.sender).transfer(amount);
        emit WithdrawETH(msg.sender, amount);
    }

    /// @notice withdraw all pending of an ERC20 token
    /// @dev msg.sender is the beneficiary
    /// @param token ERC20 token address
    function withdrawERC20(address token) external {
        uint256 amount = s.erc20Balances[token][msg.sender];
        s.erc20Balances[token][msg.sender] = 0;
        IERC20(token).safeTransfer(msg.sender, amount);
        emit WithdrawERC20(msg.sender, token, amount);
    }

    /// @notice Fetch pending ether to withdraw for an address
    /// @param account an address
    /// @return amount of ether in wei
    function pendingETH(address account) external view returns (uint256) {
        return s.ethBalances[account];
    }

    /// @notice Fetch pending ERC20 to withdraw for an address
    /// @param account an address
    /// @param token ERC20 token address
    /// @return amount of ERC20 in unit of the token
    function pendingERC20(address account, address token)
        external
        view
        returns (uint256)
    {
        return s.erc20Balances[token][account];
    }
}
