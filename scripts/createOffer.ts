import { Contract, Signer } from "ethers";
import { ethers, network } from "hardhat";
import { writeFileSync } from "fs";
const sleep = async (ms: any) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const randomNum = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

const log = (...args: any[]) => {
  console.log(`${new Date().toLocaleTimeString()}\t ${args}`);
};

const executeFn = async (options: {
  contract: Contract;
  from: Signer;
  method: string;
  args: any[];
  overrides?: any;
}) => {
  const gasEstimated = await options.contract
    .connect(options.from)
    .estimateGas[options.method](...options.args, { ...options.overrides });

  const resp = await options.contract
    .connect(options.from)
    .functions[options.method](...options.args, {
      ...options.overrides,
      gasLimit: gasEstimated.mul(150).div(100),
    });

  return resp;
};

async function getTokenIds(options: { account: string; tokenAddress: string }) {
  const contract = await ethers.getContractAt("IERC721", options.tokenAddress);

  const received = (
    await contract.queryFilter(contract.filters.Transfer(null, options.account))
  ).map((e) => {
    return e.args ? e.args[2].toString() : null;
  });

  const transfered = (
    await contract.queryFilter(contract.filters.Transfer(options.account))
  ).map((e) => {
    return e.args ? e.args[2].toString() : null;
  });

  return received.filter((e) => {
    return !transfered.includes(e);
  });
}
async function makeOffer(options: {
  offerPrice: string | number;
  mnemonicWalletIndex: number;
  vrLandsAddress: string;
  currency: string;
  fileName: string;
}) {
  const signer = (await ethers.getSigners())[options.mnemonicWalletIndex];

  const lands = await getTokenIds({
    account: signer.address,
    tokenAddress: options.vrLandsAddress,
  });

  if (lands.length === 0) {
    throw new Error(`${signer.address} has no lands to offer`);
  }

  const vrLands = await ethers.getContractAt("IERC721", options.vrLandsAddress);
  const marketplace = await ethers.getContract("MarketplaceDiamond-Staging");
  const isApprovedForAll = await vrLands.isApprovedForAll(
    signer.address,
    marketplace.address
  );

  let signerNonce = await signer.getTransactionCount("latest");

  const erc20 = await ethers.getContractAt("IERC20", options.currency);

  const decimals = ethers.BigNumber.from(
    await signer.call({
      to: erc20.address,
      data: "0x313ce567",
    })
  );

  if (!isApprovedForAll) {
    const tx = await executeFn({
      contract: vrLands,
      from: signer,
      method: "setApprovalForAll(address,bool)",
      args: [marketplace.address, true],
      overrides: { nonce: signerNonce++ },
    });

    log(`setApprovalForAll from ${signer.address} to marketplace`);
  }

  for (const tokenId of lands) {
    const tx = await executeFn({
      contract: marketplace,
      from: signer,
      method: "createOffer(address,uint256,uint256,address)",
      args: [
        vrLands.address,
        tokenId.toString(),
        ethers.utils.parseUnits(options.offerPrice.toString(), decimals || 6),
        options.currency,
      ],
      overrides: { nonce: signerNonce++ },
    });

    log(
      `createOffer with vrLands tokenId ${tokenId}  from ${signer.address} with price ${options.offerPrice}`
    );
  }

  // write address of seller to file
  // append mode
  writeFileSync(options.fileName, `${signer.address}\n`, {
    encoding: "utf8",
    flag: "a+",
  });
}

makeOffer({
  offerPrice: Number(process.env.OFFER_PRICE) || 300,
  currency: "0x4988a896b1227218e4A686fdE5EabdcAbd91571f",
  vrLandsAddress: "0x486a92035a73de83f393DBfFa2C1a72e047203E9",
  mnemonicWalletIndex: Number(process.env.MNEMONIC_INDEX) || 0,
  fileName: process.env.SELLERS_FILE_NAME || "",
}).catch((e) => {
  log(e.message);
});
