import { Contract, Signer } from "ethers";
import { ethers, network } from "hardhat";

const sleep = async (ms: any) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const randomNum = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

const log = (...args: any[]) => {
  console.log(`${new Date().toLocaleTimeString()}\t ${args}`);
};

const executeFn = async (options: {
  contract: Contract;
  from: Signer;
  method: string;
  args: any[];
  overrides?: any;
}) => {
  const gasEstimated = await options.contract
    .connect(options.from)
    .estimateGas[options.method](...options.args, { ...options.overrides });

  const resp = await options.contract
    .connect(options.from)
    .functions[options.method](...options.args, {
      ...options.overrides,
      gasLimit: gasEstimated.mul(150).div(100),
    });

  return resp;
};

async function withdrawAndTransferTo(options: {
  mnemonicWalletIndex: number;
  currency: string;
  recipient: string;
}) {
  const signer = (await ethers.getSigners())[options.mnemonicWalletIndex];
  const contract = await ethers.getContract("MarketplaceDiamond-Staging");
  const pending = await contract.pendingERC20(signer.address, options.currency);

  if (!ethers.utils.isAddress(options.recipient)) {
    throw new Error(`invalid recipient ${options.recipient}`);
  }

  if (pending.eq(0)) {
    throw new Error(`${signer.address} does not have pending to withdraw`);
  }

  let nonce = await signer.getTransactionCount("latest");
  await executeFn({
    contract: contract,
    method: "withdrawERC20(address)",
    from: signer,
    args: [options.currency],
    overrides: { nonce: nonce++ },
  });

  const token = await ethers.getContractAt("IERC20", options.currency);
  await executeFn({
    contract: token,
    method: "transfer(address,uint256)",
    from: signer,
    args: [options.recipient, pending],
    overrides: { nonce: nonce++ },
  });

  await log(
    `${signer.address} withdraw and transferred ${pending.toString()} to ${
      options.recipient
    }!`
  );
}

withdrawAndTransferTo({
  mnemonicWalletIndex: Number(process.env.MNEMONIC_INDEX) || 0,
  currency: "0x4988a896b1227218e4A686fdE5EabdcAbd91571f",
  recipient: process.env.RECIPIENT || "",
}).catch((e) => {
  log(e.message);
});
