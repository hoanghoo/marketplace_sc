import { ethers, getNamedAccounts } from "hardhat";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract("Marketplace-Prod", deployerBeta);
  const tx = await contract.transferOwnership(
    "0x2C7ec33367e3B4903AdE37c824cFe65C86698f9a"
  );
  await tx.wait();
  console.log(tx.hash);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
