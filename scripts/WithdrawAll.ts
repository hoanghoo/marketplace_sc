import { ethers, getNamedAccounts } from "hardhat";

async function main() {
  const { deployerBeta } = await getNamedAccounts();
  const contract = await ethers.getContract(
    "MarketplaceDiamond-Staging",
    deployerBeta
  );
  const tx = await contract.bump(
    "0x4988a896b1227218e4a686fde5eabdcabd91571f",
    [
      "0x1e13447b316b6de3c27077350793ccedc194b052",
      "0xf6d2d6437e5ce401bafeef2652821afa57980589",
      "0x4958ced9c576b5ee74dd659316db1ef6a568fc99",
      "0x1ef9b8de7e38082d104ff45327d2308426407050",
      "0x304c90addf3163d15cab03789b53a975105c15f0",
      "0xacfc00bfaeb944226b38ae23527ae9d2509af6f2",
      "0x44145a8aae1acfcfeb06b150987016ea718155ee",
    ],
    [
      ethers.utils.parseUnits("0.25", 6),
      ethers.utils.parseUnits("2.602031", 6),
      ethers.utils.parseUnits("0.263665", 6),
      ethers.utils.parseUnits("6.402651", 6),
      ethers.utils.parseUnits("1.000028", 6),
      ethers.utils.parseUnits("2.7", 6),
      ethers.utils.parseUnits("0.343717", 6),
    ]
  );
  console.log(tx); // True
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
