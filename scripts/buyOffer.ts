import { Contract, Signer } from "ethers";
import { ethers, network } from "hardhat";

const sleep = async (ms: any) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const randomNum = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

const log = (...args: any[]) => {
  console.log(`${new Date().toLocaleTimeString()}\t ${args}`);
};

const faucetForTest = async (account: string) => {
  if (network.name === "hardhat") {
    await network.provider.send("hardhat_setBalance", [
      account,
      ethers.utils.parseEther("10").toHexString(),
    ]);
  }
};

const executeFn = async (options: {
  contract: Contract;
  from: Signer;
  method: string;
  args: any[];
  overrides?: any;
}) => {
  const gasEstimated = await options.contract
    .connect(options.from)
    .estimateGas[options.method](...options.args, { ...options.overrides });

  const resp = await options.contract
    .connect(options.from)
    .functions[options.method](...options.args, {
      ...options.overrides,
      gasLimit: gasEstimated.mul(150).div(100),
    });

  return resp;
};

const openningOffers = async (options: {
  contract: Contract;
  seller: string;
}) => {
  const offerFilter = options.contract.filters.OfferCreated(
    null,
    null,
    null,
    options.seller,
    null,
    null
  );

  const creates = (await options.contract.queryFilter(offerFilter)).map((e) => {
    return e.args?.offerId.toString();
  });

  const buyFilter = options.contract.filters.OfferClosed(
    null,
    options.seller,
    null,
    null,
    null
  );

  const matched = (await options.contract.queryFilter(buyFilter)).map((e) => {
    return e.args?.offerId.toString();
  });

  return creates.filter((e) => {
    return !matched.includes(e);
  });
};

async function buyOffers(options: {
  motherWalletPriv: string;
  mnemonicWalletIndex: number;
  currency: string;
  seller: string;
}) {
  const signer = (await ethers.getSigners())[options.mnemonicWalletIndex];
  const contract = await ethers.getContract("MarketplaceDiamond-Dev");

  const motherWalletSigner = new ethers.Wallet(
    options.motherWalletPriv,
    contract.provider
  );

  if (!ethers.utils.isAddress(options.seller)) {
    throw new Error(`invalid recipient ${options.seller}`);
  }

  if (!ethers.Signer.isSigner(motherWalletSigner)) {
    throw new Error(`invalid mother wallet priv ${options.motherWalletPriv}`);
  }

  const offerIds = await openningOffers({
    contract: contract,
    seller: options.seller,
  });

  await faucetForTest(motherWalletSigner.address);

  let totalAmount = ethers.BigNumber.from(0);

  const offerToBuy = [];

  // get total amount to transfer from mother wallet
  for (const offerId of offerIds) {
    const offer = await contract.getOffer(offerId);
    if (!offer.closed && offer.currency === options.currency) {
      totalAmount = totalAmount.add(offer.price);
      offerToBuy.push(offerId);
    }
  }

  const tkn = await ethers.getContractAt("IERC20", options.currency);
  await executeFn({
    contract: tkn,
    method: "transfer(address,uint256)",
    from: motherWalletSigner,
    args: [signer.address, totalAmount],
    overrides: { gasLimit: 3e5 },
  });

  let nonce = await signer.getTransactionCount("latest");

  await executeFn({
    contract: tkn,
    method: "approve(address,uint256)",
    from: signer,
    args: [contract.address, totalAmount],
    overrides: { nonce: nonce++ },
  });

  for (const offerId of offerToBuy) {
    await executeFn({
      contract: contract,
      method: "buyOffer(uint256)",
      from: signer,
      args: [offerId],
      overrides: { nonce: nonce++ },
    });

    log(`${signer.address} buy offerId ${offerId} from ${options.seller}`);
  }
}

buyOffers({
  mnemonicWalletIndex: Number(process.env.MNEMONIC_INDEX) || 0,
  currency: "0xae746294c763ACCbC5837d9e4D61a16194dd16bE",
  motherWalletPriv:
    process.env.MOTHER_WALLET_PRIV ||
    "61ba6e2df365e55ba1960f220a1a00d8e767f261bfe9f35df07c4627067d81c3",
  seller:
    process.env.SELLER_ADDRESS || "0x2ca630BB29986A3F5087528fD051D6bBe71F8E71",
}).catch((e) => {
  log(e.message);
});
